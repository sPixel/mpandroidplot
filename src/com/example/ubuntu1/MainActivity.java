package com.example.ubuntu1;
/*
 * @author Nazım YILDIZ
 * @date 23.09.2015
 * @brief Eclipse Luna v4.4.2 and android-sdk-linux_r24.3.4
 * @note For details: https://github.com/PhilJay/MPAndroidChart
 * */

import java.util.ArrayList;

import com.example.ubuntu1.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.ChartInterface;
import com.github.mikephil.charting.utils.ColorTemplate;

import android.R.bool;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnTouchListener {


	LineChart 	chart;
	TextView 	textGeneral;
	ArrayList<String> xVals;
	CheckBox checkId, checkIq, checkIa, checkIb;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		chartSettings();
		controlInits();
		autoDrawing();
	}

	public void chartSettings(){
		int[] colors = new int[4];
		String[] labels = new String[4];
		ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();	//4 tane data kurcaz...

		colors[0] = Color.YELLOW; 
		colors[1] = Color.GREEN;	
		colors[2] = Color.RED;
		colors[3] = Color.BLUE;
		labels[0] = "d Current";	
		labels[1] = "q Current";	
		labels[2] = "Ia(t)";
		labels[3] = "Ib(t)";
		
		chart = (LineChart) findViewById(R.id.chart);
		chart.setBackgroundColor(Color.LTGRAY);
		chart.setDescription("Motor d & q Currents");
		chart.setDrawBorders(true);
		chart.setBorderColor(Color.CYAN);
		chart.setTouchEnabled(true);
		//chart.setOnChartValueSelectedListener(this);
		chart.setPinchZoom(false);	        // if disabled, scaling can be done on x- and y-axis separately
		Legend l = chart.getLegend();
        l.setPosition(LegendPosition.BELOW_CHART_LEFT);		//Basilan veri kumelerinin tanimlari
        l.setCustom(colors, labels);
//        chart.setMaxVisibleValueCount(1000);		//??? ne ise yaradigini anlamadim.
        chart.setKeepScreenOn(false);
        chart.getAxisLeft().setAxisMaxValue(2048.0f);
        chart.getAxisLeft().setAxisMinValue(-2048.0f);
        chart.getAxisRight().setAxisMaxValue(30.0f);
        chart.getAxisRight().setAxisMinValue(-30.0f);
        
        xVals = new ArrayList<String>();

        for (int z = 0; z < 4; z++) {

            LineDataSet d = new LineDataSet(null, "DataSet " + (z + 1));
            d.setLineWidth(1.5f);
            d.setCircleSize(0.5f);
            
            dataSets.add(d);
            
            dataSets.get(z).setColor(colors[z]);
            dataSets.get(z).setCircleColor(colors[z]);
            dataSets.get(z).setValueTextColor(colors[z]);
        }

        dataSets.get(0).enableDashedLine(10, 10, 0);
        
        LineData data = new LineData(xVals, dataSets);
		chart.setData(data);
		chart.getData().setDrawValues(true);
		chart.notifyDataSetChanged(); // chart objesine datalarin degistigi bildiriliyor
		chart.invalidate();			//grafik tazeleniyor
	}
	
	public void controlInits(){
		textGeneral	=	(TextView)findViewById(R.id.textGeneral);
		textGeneral.setTextColor(Color.DKGRAY);
		textGeneral.setText("Lineer Çizim Demo");
		
		checkIa		=	(CheckBox)findViewById(R.id.checkIa);
		checkIb		=	(CheckBox)findViewById(R.id.checkIb);
		checkId		=	(CheckBox)findViewById(R.id.checkId);
		checkIq		=	(CheckBox)findViewById(R.id.checkIq);
		checkIa.setChecked(true);
		checkIb.setChecked(true);
		checkId.setChecked(true);
		checkIq.setChecked(true);
	}
	

    private void addEntry() {

    	//Grafige basilacak olan data nesnesi aliniyor
    	//Burada data tek bir fonksiyon grafigi degil kurulmus olan tum fonksiyonlarin
    	//referansi tutuluyor.
        LineData data = chart.getData();
        
        if(data.getXValCount() > 30000){
        	removeEntry();
        	xVals.clear();
        	chartSettings();
        	return;
        }
        
        // once yeni bir x verisi ekleyelim
        data.addXValue((data.getXValCount()+1) + "");
        //Yeni eklenen x versine karsilik gelen y degerlerini ekleyelim her bir f(x) icin
        //bu uygulama icin 3 grafik aynı plot ekrani icinde cizdiriliyor.
        for(int currents = 0; currents < 4; currents++){
            if(data != null) {

                
                if(currents == 0 & checkId.isChecked())
                	data.addEntry(new Entry((float) (Math.random() * 200.0f), data.getXValCount()), currents);
                else if(currents == 1 & checkIq.isChecked())
                	data.addEntry(new Entry((float) (Math.random() * 600.0f) + 200.0f, data.getXValCount()), currents);
                else if(currents == 2 & checkIa.isChecked())
                	data.addEntry(new Entry((float)Math.sin(data.getXValCount() * 0.0174f) * 1000, data.getXValCount()), currents);
                else if(currents == 3 & checkIb.isChecked())
                	data.addEntry(new Entry((float)Math.sin(data.getXValCount() * 0.0174f + 2.0933f) * 1000.0f, data.getXValCount()), currents);  
            }	
            else
            	chartSettings();
            
        }
    }
    
    private void updatePlot(){
    	LineData data = chart.getData();
    	
        // let the chart know it's data has changed
        chart.notifyDataSetChanged();
      
        // this automatically refreshes the chart (calls invalidate())
        chart.moveViewTo(data.getXValCount()-7, 50f, AxisDependency.LEFT);
    }

    private boolean removeEntry(){
    	boolean	isRemoved;
    	isRemoved = false;
    	
    	chart.getData().clearValues();
    	for (int i = 0; i <  chart.getData().getDataSetCount(); i++) 
    		chart.getData().getDataSetByIndex(i).clear();
    	   
    	if(chart.getData() == null)
    		isRemoved = true;
    	return isRemoved;
    }
    
    @Override
    public boolean onTouch(View v, MotionEvent event) {
    	// TODO Auto-generated method stub
    	return false;
    }
    
    private void autoDrawing() {
     
        new Thread() {
            @Override
            public void run() {

            	while(true){
                    try {
    					Thread.sleep(100);
    				} catch (InterruptedException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
                    try {

                        // code runs in a thread
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            	for (int i = 0; i < 10; i++) {
                            		addEntry();
								}
                            	updatePlot();
                                
                            }
                        });
                    } catch (final Exception ex) {
                       
                    }
            	}

            }
        }.start();

    }
}